'use strict';
/******************************************************************************/
/*       ______                                                               */
/*      //   ) )                                                              */
/*     //___/ /            __    __  ___  __      ___       __     ( )        */
/*    / __  (   //   / / //  ) )  / /   //  ) ) //   ) ) //   ) ) / / \\ / /  */
/*   //    ) ) //   / / //       / /   //      //   / / //   / / / /   \/ /   */
/*  //____/ / ((___( ( //       / /   //      ((___/ / //   / / / /    / /\   */
/*                                                                            */
/******************************************************************************/

  /*----- Load Gulp, Plugins and Requirements --------------------------------*/

    var gulp = require('gulp');
    var $ = require('gulp-load-plugins')({
      rename: {
        'gulp-gh-pages': 'ghpages'
      }
    });

    var browserSync = require('browser-sync');
    var del = require('del');
    var opt = false;

  /*----- Task clean ---------------------------------------------------------*/

    gulp.task('clean', function (cb) {
      del(['build', '.tmp'], cb);
    });

/*----------------------------------------------------------------------------*/
/* Data                                                                       */
/*----------------------------------------------------------------------------*/

  /*----- Data YAML to JSON --------------------------------------------------*/

    gulp.task('dataYaml2Json', function(cb) {

      return gulp.src('../indicators/*.yml')
        .pipe($.yaml({ schema: 'DEFAULT_SAFE_SCHEMA' }))
        .pipe(gulp.dest('.tmp/json'), cb);

    });

  /*----- Task Build JSON ----------------------------------------------------*/

    gulp.task('dataJson', ['dataYaml2Json'], function() {

      gulp.src('.tmp/json/indicators-*.json')
        .pipe($.jsoncombine('indicators.json', function (data) {
          return new Buffer(JSON.stringify(data, null, 4));
        }))
        .pipe($.jsonlint())
        .pipe($.jsonlint.reporter())
        .pipe(gulp.dest('.tmp/json'))
        .pipe($.jsonminify())
        .pipe(gulp.dest('app/json'));

    });

  /*----- Task data ----------------------------------------------------------*/

    gulp.task('data', ['dataJson'], function(cb) { cb(); });

/*----------------------------------------------------------------------------*/
/* Linting                                                                    */
/*----------------------------------------------------------------------------*/

  /*----- Task lintGulpfile --------------------------------------------------*/

    gulp.task('lintGulpfile', function() {

      gulp.src('gulpfile.js')

        .pipe($.eslint('.eslintrc_node'))
        .pipe($.eslint.format());

    });

  /*----- Task lintMain ------------------------------------------------------*/

    gulp.task('lintMain', function() {

      gulp.src('app/javascript/main.js')

        .pipe($.eslint('.eslintrc'))
        .pipe($.eslint.format());

    });

  /*----- Task lintJSON ------------------------------------------------------*/

    gulp.task('lintJSON', ['data'], function() {

      gulp.src('app/json/*.json')

        .pipe($.jsonlint())
        .pipe($.jsonlint.reporter());

    });

  /*----- Task lint (gulpfile.js) --------------------------------------------*/

    gulp.task('lint', ['lintGulpfile', 'lintMain', 'lintJSON'], function(cb) { cb(); });

/*----------------------------------------------------------------------------*/
/* Building                                                                   */
/*----------------------------------------------------------------------------*/

  /*----- Task build (libSass) -----------------------------------------------*/

    gulp.task('styles', function (cb) {

      browserSync.notify('Compiling CSS');

      return gulp.src('app/styles/style.scss')
        .pipe($.sourcemaps.init())
        .pipe($.sass({
          outputStyle: 'nested',
          precision: 10,
          errLogToConsole: false,
          onError: function(err) {
            console.log(err);
            browserSync.notify(err, 5000);
          }
        }))
        .pipe($.sourcemaps.write())
        .pipe($.autoprefixer())
        .pipe($.if(opt, $.cssnano()))
        .on('error', function(err) {
          console.log(err.message);
          browserSync.notify(err.message, 5000);
        })
        .pipe(gulp.dest('.tmp/styles'))
        .pipe(browserSync.reload({ stream: true }), cb);

    });

  /*----- Task scriptJquery --------------------------------------------------*/

    gulp.task('scriptJquery', function() {

      gulp.src('bower_components/jquery/dist/jquery.min.js')
        .pipe($.if(opt, $.uglify()))
        .pipe(gulp.dest('.tmp/javascript'));

    });

  /*----- Task scriptAngular -------------------------------------------------*/

    gulp.task('scriptAngular', function() {

      gulp.src('bower_components/angular/angular.min.js')
        .pipe($.if(opt, $.uglify()))
        .pipe(gulp.dest('.tmp/javascript'));

    });

  /*----- Task scriptChartist ------------------------------------------------*/

    gulp.task('scriptChartist', function() {

      gulp.src('bower_components/chartist/dist/chartist.min.js')
        .pipe($.if(opt, $.uglify()))
        .pipe(gulp.dest('.tmp/javascript'));

    });

  /*----- Task scriptAngularChartist -----------------------------------------*/

    gulp.task('scriptAngularChartist', function() {

      gulp.src('bower_components/angular-chartist.js/dist/angular-chartist.min.js')
        .pipe($.if(opt, $.uglify()))
        .pipe(gulp.dest('.tmp/javascript'));

    });

  /*----- Task scriptMain ----------------------------------------------------*/

    gulp.task('scriptMain', function() {

      gulp.src('app/javascript/main.js')
        .pipe($.if(opt, $.uglify()))
        .pipe(gulp.dest('.tmp/javascript'))
        .pipe(browserSync.reload({ stream: true }));

    });

  /*----- Task scriptJSON ----------------------------------------------------*/

    gulp.task('scriptJSON', function() {

      gulp.src('app/json/*.json')
        .pipe($.if(opt, $.jsonminify()))
        .pipe($.if(opt, gulp.dest('build')))
        .pipe(gulp.dest('.tmp'))
        .pipe(browserSync.reload({ stream: true }));

    });

  /*----- CNAME --------------------------------------------------------------*/

    gulp.task('scriptCNAME', function() {

      gulp.src('app/CNAME')
        .pipe($.if(opt, gulp.dest('build')));

    });

  /*----- Task scripts -------------------------------------------------------*/

    gulp.task('scripts', [
      'scriptJquery',
      'scriptAngular',
      'scriptChartist',
      'scriptAngularChartist',
      'scriptMain',
      'scriptJSON',
      'scriptCNAME'
    ], function(cb) { cb(); });

  /*----- Task images ---------------------------------------------------------*/

    gulp.task('images', function() {

      return gulp.src(['app/images/*'])
        .pipe(gulp.dest('.tmp/images'));

    });

  /*----- Task html ----------------------------------------------------------*/

    gulp.task('html', function() {

      return gulp.src(['app/index.html'])
        .pipe(gulp.dest('.tmp'));

    });

  /*----- Task index ---------------------------------------------------------*/

    gulp.task('index', ['lint', 'styles', 'scripts', 'images', 'html'], function () {

      var css = gulp.src('.tmp/styles/style.css');
      var jsJq = gulp.src('.tmp/javascript/jquery.min.js');
      var jsA = gulp.src('.tmp/javascript/angular.min.js');
      var jsC = gulp.src('.tmp/javascript/chartist.min.js');
      var jsAC = gulp.src('.tmp/javascript/angular-chartist.min.js');
      var jsMain = gulp.src('.tmp/javascript/main.js');

      return gulp.src('.tmp/index.html')

        .pipe($.if(opt, $.inject(css, {
          starttag: '<!-- inject:inlinecss:css -->',
          transform: function (filePath, file) {
            return '<style type="text/css">' + file.contents.toString('utf8') + '</style>';
          }
        })))

        // JQuery
        .pipe($.if(opt, $.inject(jsJq, {
          starttag: '<!-- inject:inlinejqjs:js -->',
          transform: function (filePath, file) {
            return '<script>' + file.contents.toString('utf8') + '</script>';
          }
        })))

        // Angular
        .pipe($.if(opt, $.inject(jsA, {
          starttag: '<!-- inject:inlineajs:js -->',
          transform: function (filePath, file) {
            return '<script>' + file.contents.toString('utf8') + '</script>';
          }
        })))

        // Chartist
        .pipe($.if(opt, $.inject(jsC, {
          starttag: '<!-- inject:inlinecjs:js -->',
          transform: function (filePath, file) {
            return '<script>' + file.contents.toString('utf8') + '</script>';
          }
        })))

        // Angular Chartist
        .pipe($.if(opt, $.inject(jsAC, {
          starttag: '<!-- inject:inlineacjs:js -->',
          transform: function (filePath, file) {
            return '<script>' + file.contents.toString('utf8') + '</script>';
          }
        })))

        // Main
        .pipe($.if(opt, $.inject(jsMain, {
          starttag: '<!-- inject:inlinejs:js -->',
          transform: function (filePath, file) {
            return '<script>' + file.contents.toString('utf8') + '</script>';
          }
        })))

        .pipe($.if(opt, $.img64()))
        .pipe($.if(opt, $.htmlmin({
          collapseWhitespace: true,
          removeComments: true,
          removeCommentsFromCDATA: true,
          removeCDATASectionsFromCDATA: true,
          collapseBooleanAttributes: true,
          removeAttributeQuotes: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true,
          removeOptionalTags: true,
          caseSensitive: true,
          minifyJS: true,
          minifyCSS: true
        })))
        .pipe($.if(opt, gulp.dest('build')))
        .pipe(browserSync.reload({ stream: true }));

    });

/*----------------------------------------------------------------------------*/
/* Tasks: Browser-Sync and Watch                                              */
/*----------------------------------------------------------------------------*/

  /*----- Task browserSync ---------------------------------------------------*/

    gulp.task('sync', ['index'], function() {
      browserSync({
        server: { baseDir: '.tmp' }
      });
    });

  /*----- Task watch ---------------------------------------------------------*/

    gulp.task('watch', function() {

      // Sass files:
      gulp.watch('app/styles/**/*', ['styles']);

      // Script files:
      gulp.watch('app/javascript/**/*', ['lintMain', 'scripts']);

      // JSON files:
      gulp.watch('app/json/**/*', ['lintJSON', 'scripts']);

      // Images files:
      gulp.watch('app/images/**/*', ['images']);

      // Script files:
      gulp.watch('app/index.html', ['index']);

    });

/*----------------------------------------------------------------------------*/
/* Tasks: Deploy to GitHub Pages                                              */
/*----------------------------------------------------------------------------*/

    gulp.task('ghpages', ['index'], function() {
      return gulp.src('./build/**/*')
        .pipe($.ghpages());
    });

/*----------------------------------------------------------------------------*/
/* Execution                                                                  */
/*----------------------------------------------------------------------------*/

    gulp.task('build', function () { opt = true; gulp.start('index'); });
    gulp.task('deploy', function () { opt = true; gulp.start('ghpages'); });
    gulp.task('default', ['sync', 'watch']);

// /*----------------------------------------------------------------------------*/
