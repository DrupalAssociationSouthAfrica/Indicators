'use strict';
/******************************************************************************/
/*       ______                                                               */
/*      //   ) )                                                              */
/*     //___/ /            __    __  ___  __      ___       __     ( )        */
/*    / __  (   //   / / //  ) )  / /   //  ) ) //   ) ) //   ) ) / / \\ / /  */
/*   //    ) ) //   / / //       / /   //      //   / / //   / / / /   \/ /   */
/*  //____/ / ((___( ( //       / /   //      ((___/ / //   / / / /    / /\   */
/*                                                                            */
/******************************************************************************/

//----- Angular Chart --------------------------------------------------------//

  (function(angular) {

    angular.module('drupalChart', ['angular-chartist'])

    .controller('ChartController', ['$scope', '$filter', '$http', '$timeout', function($scope, $filter, $http, $timeout) {

      // Variables
      var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      var blankSeries = [ [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] ];

      // Initial Values.
      $scope.primary = 'community';

      // Descriptions.
      $scope.descriptions = {
        'community': 'C',
        'membership': 'M',
        'finance': 'F',
        'adoption': 'A',
      };

      // Inject Math ;-)
      $scope.Math = window.Math;

      // Default Chart Options.
      $scope.lineOptions = {
        showArea: true,
        fullWidth: true,
        axisX: {
          labelOffset: {
            x: -12,
            y: 0
          }
        }
      };

      // Charts (Blank Placeholders)
      $scope.chartsData = new Object();
      $scope.chartsData.community = {
        labels: months,
        series: blankSeries
      };
      $scope.chartsData.membership = {
        labels: months,
        series: blankSeries
      };
      $scope.chartsData.finance = {
        labels: months,
        series: blankSeries
      };
      $scope.chartsData.adoption = {
        labels: months,
        series: blankSeries
      };

      // Retrieve Community data.
      function getCommunityData(raw, community) {

        // Collect relevant and available data from raw.

        var collect = new Object();
        var year = null;
        var month = null;
        var minYear = null;
        var maxYear = null;
        community.series = [];
        community.ledgend = [];

        for (var key in raw) {

          // Skip loop if the property is from prototype.
          if (!raw.hasOwnProperty(key)) continue;

          // Year and Month.
          year = parseInt(raw[key].month.slice(0, 4));
          minYear = minYear === null ? year : Math.min(minYear, year);
          maxYear = maxYear === null ? year : Math.max(maxYear, year);
          month = parseInt(raw[key].month.slice(5));

          // Collect.
          if (typeof collect[year] === 'undefined') {
            collect[year] = new Object();
          }
          if (typeof collect[year][month] === 'undefined') {
            collect[year][month] = new Object();
          }
          collect[year][month].capeTown = (typeof raw[key].community.meetups.cape_town === 'undefined') ? null : raw[key].community.meetups.cape_town.membership;
          collect[year][month].durban = (typeof raw[key].community.meetups.durban === 'undefined') ? null : raw[key].community.meetups.durban.membership;
          collect[year][month].johannesburgBryanston = (typeof raw[key].community.meetups.johannesburg_bryanston === 'undefined') ? null : raw[key].community.meetups.johannesburg_bryanston.membership;
          collect[year][month].johannesburgParkhurst = (typeof raw[key].community.meetups.johannesburg_parkhurst === 'undefined') ? null : raw[key].community.meetups.johannesburg_parkhurst.membership;
          collect[year][month].pretoriaArcadia = (typeof raw[key].community.meetups.pretoria_arcadia === 'undefined') ? null : raw[key].community.meetups.pretoria_arcadia.membership;
          collect[year][month].pretoriaCenturion = (typeof raw[key].community.meetups.pretoria_centurion === 'undefined') ? null : raw[key].community.meetups.pretoria_centurion.membership;

        };

        console.log(collect);

        // Massage the collected data into complete series data for Chartist and
        // presentational requirements.

        var asciiOffset = 0;
        var seriesAlpha = null;

        var seriesCapeTown = {};
        var seriesDurban = {};
        var seriesJohannesburgBryanston = {};
        var seriesJohannesburgParkhurst = {};
        var seriesPretoriaArcadia = {};
        var seriesPretoriaCenturion = {};

        for (year = minYear; year <= maxYear; year++) {

          for (month = 1; month <= 12; month++) {

            if (typeof seriesCapeTown[month] === 'undefined' || seriesCapeTown[month] === null) {
              seriesCapeTown[month] = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].capeTown;
            }
            if (typeof seriesDurban[month] === 'undefined' || seriesDurban[month] === null) {
              seriesDurban[month] = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].durban;
            }
            if (typeof seriesJohannesburgBryanston[month] === 'undefined' || seriesJohannesburgBryanston[month] === null) {
              seriesJohannesburgBryanston[month] = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].johannesburgBryanston;
            }
            if (typeof seriesJohannesburgParkhurst[month] === 'undefined' || seriesJohannesburgParkhurst[month] === null) {
              seriesJohannesburgParkhurst[month] = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].johannesburgParkhurst;
            }
            if (typeof seriesPretoriaArcadia[month] === 'undefined' || seriesPretoriaArcadia[month] === null) {
              seriesPretoriaArcadia[month] = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].pretoriaArcadia;
            }
            if (typeof seriesPretoriaCenturion[month] === 'undefined' || seriesPretoriaCenturion[month] === null) {
              seriesPretoriaCenturion[month] = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].pretoriaCenturion;
            }

          }

        }

        var seriesCapeTown = $.map(seriesCapeTown, function(value, index) {
          return [value];
        });
        var seriesDurban = $.map(seriesDurban, function(value, index) {
          return [value];
        });
        var seriesJohannesburgBryanston = $.map(seriesJohannesburgBryanston, function(value, index) {
          return [value];
        });
        var seriesJohannesburgParkhurst = $.map(seriesJohannesburgParkhurst, function(value, index) {
          return [value];
        });
        var seriesPretoriaArcadia = $.map(seriesPretoriaArcadia, function(value, index) {
          return [value];
        });
        var seriesPretoriaCenturion = $.map(seriesPretoriaCenturion, function(value, index) {
          return [value];
        });

        // Cape Town series data for Chartist.
        seriesAlpha = String.fromCharCode(97 + asciiOffset);
        community.series.push(seriesCapeTown);
        var ledgend = new Object();
        ledgend.cssClass = seriesAlpha;
        ledgend.textLabel = 'Cape Town';
        community.ledgend.push(ledgend);
        asciiOffset++;

        // Durban series data for Chartist.
        seriesAlpha = String.fromCharCode(97 + asciiOffset);
        community.series.push(seriesDurban);
        var ledgend = new Object();
        ledgend.cssClass = seriesAlpha;
        ledgend.textLabel = 'Durban';
        community.ledgend.push(ledgend);
        asciiOffset++;

        // Johannesburg Bryanston series data for Chartist.
        seriesAlpha = String.fromCharCode(97 + asciiOffset);
        community.series.push(seriesJohannesburgBryanston);
        var ledgend = new Object();
        ledgend.cssClass = seriesAlpha;
        ledgend.textLabel = 'Johannesburg Bryanston';
        community.ledgend.push(ledgend);
        asciiOffset++;

        // Johannesburg Parkhurst series data for Chartist.
        seriesAlpha = String.fromCharCode(97 + asciiOffset);
        community.series.push(seriesJohannesburgParkhurst);
        var ledgend = new Object();
        ledgend.cssClass = seriesAlpha;
        ledgend.textLabel = 'Johannesburg Parkhurst';
        community.ledgend.push(ledgend);
        asciiOffset++;

        // Pretoria Arcadia series data for Chartist.
        seriesAlpha = String.fromCharCode(97 + asciiOffset);
        community.series.push(seriesPretoriaArcadia);
        var ledgend = new Object();
        ledgend.cssClass = seriesAlpha;
        ledgend.textLabel = 'Pretoria Arcadia';
        community.ledgend.push(ledgend);
        asciiOffset++;

        // Pretoria Centurion series data for Chartist.
        seriesAlpha = String.fromCharCode(97 + asciiOffset);
        community.series.push(seriesPretoriaCenturion);
        var ledgend = new Object();
        ledgend.cssClass = seriesAlpha;
        ledgend.textLabel = 'Pretoria Centurion';
        community.ledgend.push(ledgend);
        asciiOffset++;



        // Description.
        community.description = 'Meetup.com mebership count.';

        console.log(community);

      }

      // Retrieve Membership data.
      function getMembershipData(raw, membership) {

        // Collect relevant and available data from raw.

        var collect = new Object();
        var year = null;
        var month = null;
        var minYear = null;
        var maxYear = null;
        membership.series = [];
        membership.ledgend = [];

        for (var key in raw) {

          // Skip loop if the property is from prototype.
          if (!raw.hasOwnProperty(key)) continue;

          // Year and Month.
          year = parseInt(raw[key].month.slice(0, 4));
          minYear = minYear === null ? year : Math.min(minYear, year);
          maxYear = maxYear === null ? year : Math.max(maxYear, year);
          month = parseInt(raw[key].month.slice(5));

          // Collect.
          if (typeof collect[year] === 'undefined') {
            collect[year] = new Object();
          }
          if (typeof collect[year][month] === 'undefined') {
            collect[year][month] = new Object();
          }
          collect[year][month].dasaIndividual = raw[key].community.dasa_membership.individual.business + raw[key].community.dasa_membership.individual.government + raw[key].community.dasa_membership.individual.education;
          collect[year][month].dasaOrganisation = raw[key].community.dasa_membership.agency.business + raw[key].community.dasa_membership.agency.government + raw[key].community.dasa_membership.agency.education;
          collect[year][month].daIndividual = raw[key].community.da_membership.individual;
          collect[year][month].daOrganisation = raw[key].community.da_membership.agency;

        };

        // Massage the collected data into complete series data for Chartist and
        // presentational requirements.

        var asciiOffset = 0;
        var seriesAlpha = null;
        for (year = minYear; year <= maxYear; year++) {

          // Undefined missing values and compile series data for Chartist.
          var seriesDasaIndividual = [];
          var seriesDasaOrganisation = [];
          var seriesDaIndividual = [];
          var seriesDaOrganisation = [];
          for (month = 1; month <= 12; month++) {

            var dasaIndividual = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].dasaIndividual;
            seriesDasaIndividual.push(dasaIndividual);

            var dasaOrganisation = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].dasaOrganisation;
            seriesDasaOrganisation.push(dasaOrganisation);

            var daIndividual = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].daIndividual;
            seriesDaIndividual.push(daIndividual);

            var daOrganisation = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].daOrganisation;
            seriesDaOrganisation.push(daOrganisation);

          }

          // DASA Individual series data for Chartist.
          seriesAlpha = String.fromCharCode(97 + asciiOffset);
          membership.series.push(seriesDasaIndividual);
          var ledgend = new Object();
          ledgend.cssClass = seriesAlpha;
          ledgend.textLabel = year + ' DASA Individual';
          membership.ledgend.push(ledgend);
          asciiOffset++;

          // DASA Organisation series data for Chartist.
          seriesAlpha = String.fromCharCode(97 + asciiOffset);
          membership.series.push(seriesDasaOrganisation);
          var ledgend = new Object();
          ledgend.cssClass = seriesAlpha;
          ledgend.textLabel = year + ' DASA Organisation';
          membership.ledgend.push(ledgend);
          asciiOffset++;

          // DA Organisation series data for Chartist.
          seriesAlpha = String.fromCharCode(97 + asciiOffset);
          membership.series.push(seriesDaIndividual);
          var ledgend = new Object();
          ledgend.cssClass = seriesAlpha;
          ledgend.textLabel = year + ' DA Individual';
          membership.ledgend.push(ledgend);
          asciiOffset++;

          // DA Organisation series data for Chartist.
          seriesAlpha = String.fromCharCode(97 + asciiOffset);
          membership.series.push(seriesDaOrganisation);
          var ledgend = new Object();
          ledgend.cssClass = seriesAlpha;
          ledgend.textLabel = year + ' DA Organisation';
          membership.ledgend.push(ledgend);
          asciiOffset++;

        }

        // Description.
        membership.description = 'Membership count for Drupal Association South Africa and International.';

      }

      // Retrieve Finance data.
      function getFinanceData(raw, finance) {

        // Collect relevant and available data from raw.

        var collect = new Object();
        var year = null;
        var month = null;
        var minYear = null;
        var maxYear = null;
        finance.series = [];
        finance.ledgend = [];

        for (var key in raw) {

          // Skip loop if the property is from prototype.
          if (!raw.hasOwnProperty(key)) continue;

          // Year and Month.
          year = parseInt(raw[key].month.slice(0, 4));
          minYear = minYear === null ? year : Math.min(minYear, year);
          maxYear = maxYear === null ? year : Math.max(maxYear, year);
          month = parseInt(raw[key].month.slice(5));

          // Collect.
          if (typeof collect[year] === 'undefined') {
            collect[year] = new Object();
          }
          if (typeof collect[year][month] === 'undefined') {
            collect[year][month] = new Object();
          }
          collect[year][month].income = raw[key].organisation.income;
          collect[year][month].expenses = raw[key].organisation.expenses;

        };

        // Massage the collected data into complete series data for Chartist and
        // presentational requirements.

        var asciiOffset = 0;
        var seriesAlpha = null;
        for (year = minYear; year <= maxYear; year++) {

          // Undefined missing values and compile series data for Chartist.
          var seriesIncome = [];
          var seriesExpenses = [];
          for (month = 1; month <= 12; month++) {
            var income = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].income;
            seriesIncome.push(income);
            var expenses = (typeof collect[year][month] === 'undefined') ? null : collect[year][month].expenses;
            seriesExpenses.push(expenses);
          }

          // Income series data for Chartist.
          seriesAlpha = String.fromCharCode(97 + asciiOffset);
          finance.series.push(seriesIncome);
          var ledgend = new Object();
          ledgend.cssClass = seriesAlpha;
          ledgend.textLabel = year + ' Income';
          finance.ledgend.push(ledgend);
          asciiOffset++;

          // Expenses series data for Chartist.
          seriesAlpha = String.fromCharCode(97 + asciiOffset);
          finance.series.push(seriesExpenses);
          var ledgend = new Object();
          ledgend.cssClass = seriesAlpha;
          ledgend.textLabel = year + ' Expenses';
          finance.ledgend.push(ledgend);
          asciiOffset++;

        }

        // Description.
        finance.description = 'Income and expenses in South African Rand (ZAR).';

      }

      // Retrieve Adoption data.
      function getAdoptionData(raw, adoption) {

        // Collect relevant and available data from raw.

        var collect = new Object();
        var year = null;
        var month = null;
        var minYear = null;
        var maxYear = null;
        adoption.series = [];
        adoption.ledgend = [];

        for (var key in raw) {

          // Skip loop if the property is from prototype.
          if (!raw.hasOwnProperty(key)) continue;

          // Year and Month.
          year = parseInt(raw[key].month.slice(0, 4));
          minYear = minYear === null ? year : Math.min(minYear, year);
          maxYear = maxYear === null ? year : Math.max(maxYear, year);
          month = parseInt(raw[key].month.slice(5));

          // Collect.
          if (typeof collect[year] === 'undefined') {
            collect[year] = new Object();
          }
          collect[year][month] = raw[key].community.za_drupal_sites_in_top_3000;
        };

        // Massage the collected data into complete series data for Chartist and
        // presentational requirements.

        var asciiOffset = 0;
        var seriesAlpha = null;
        for (year = minYear; year <= maxYear; year++) {

          // Series alphabetic for styling.
          seriesAlpha = String.fromCharCode(97 + asciiOffset);

          // Undefined missing values and compile series data for Chartist.
          var series = [];
          for (month = 1; month <= 12; month++) {

            series.push(collect[year][month]);

          }

          // Series data for Chartist.
          adoption.series.push(series);

          // Ledgend.
          var ledgend = new Object();
          ledgend.cssClass = seriesAlpha;
          ledgend.textLabel = year;
          adoption.ledgend.push(ledgend);

          asciiOffset++;

        }

        // Description.
        adoption.description = 'Number of .za sites in top 3 000 .za sites that run Drupal.';

      }

      // When the DOM is ready, load JSON.
      $timeout(function() {
        $http.get('indicators.json').then(function(res) {

          $scope.rawData = res.data;

          // Date
          $scope.date = $scope.rawData[Object.keys($scope.rawData)[Object.keys($scope.rawData).length - 1]]['month'];

          // Community
          getCommunityData($scope.rawData, $scope.chartsData.community);

          // Membership
          getMembershipData($scope.rawData, $scope.chartsData.membership);

          // Finance
          getFinanceData($scope.rawData, $scope.chartsData.finance);

          // Adoption
          getAdoptionData($scope.rawData, $scope.chartsData.adoption);

        });
      }, 0);





    }]);

  })(window.angular);

//----------------------------------------------------------------------------//
