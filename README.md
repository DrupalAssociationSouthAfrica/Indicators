# Drupal Association South Africa

## Indicators / Metrics

### Monthly

- On the 1st of every month, create a new ```indicators-yyyy-mm.yml``` file in
  the ```indicators``` directory.
  - You can copy the last month's, but be careful to add new data or leave
    fields empty for all fields with / without data for the new month.
- Contact the relevant people for the data:
  - Meetup.com for the meet-up subscriber counts.
  - Robin for the newsletter subscriber count.
  - Riaan for the adoption.
  - Meet-up organisers for the headcounts.
  - The Treasurer for the income and expenses.
